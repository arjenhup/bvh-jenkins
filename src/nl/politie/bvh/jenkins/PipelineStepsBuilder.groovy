package nl.politie.bvh.jenkins

def create()
{
	echo 'Creating PipelineSteps.'
    def pipelineSteps = new PipelineSteps()
    pipelineSteps.initialize()
	return pipelineSteps
}