package nl.politie.bvh.jenkins

import nl.politie.bvh.jenkins.Constants

// Constructor, called from PipelineBuilder.initialize().
void initialize() {
    echo 'Initializing PipelineSteps.'
}

void cleanWorkspace() {
    echo 'Cleaning workspace.'
    deleteDir()
}

void checkout() {
    echo 'Checkout sources.'
	checkout scm
}

void stashSources(String name) {
    echo 'Stashing sources: ' + name + '.'
	stash name: name, excludes: '.git'
}

void stashBinaries(String name) {
    echo 'Stashing binaries: ' + name + '.'
    stash name: name, excludes: '.git'
}

void configure() {
    echo 'Configure.'
}

String determineVersion(String pomFile) {
    //return Constants.MAJOR_VERSION_NUMBER + '.' + env.BUILD_NUMBER;
    version(readFile(pomFile))
}

void archiveTestResults() {
    step([$class: 'JUnitResultArchiver', testResults: '**/target/**/TEST*.xml', allowEmptyResults: true])
}

void archiveArtifacts() {
    step([$class: 'ArtifactArchiver', artifacts: '**/target/*.jar, **/target/*.war, **/target/*.ear', fingerprint: true])
}

void runSonarAnalysis() {
    echo 'Sonar analysis temporarily disabled.'
    //echo 'Running Sonar analysis'
    // mvn "-f ${Constants.APP_MAIN_POM} -B -V -U -e org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar -Dsonar.host.url='${Constants.SONAR_URL}'"
}

void deployToNexus() {
    echo 'Deploying to Nexus temporarily disabled.'
    //echo 'Deploying to Nexus.'
    //mvn "-f '${Constants.APP_MAIN_POM}' -B -V -U -e clean source:jar javadoc:jar deploy -DskipTests"
}

void deployApplicationToDev()
{
    echo 'Deploying App to Dev environment (WebLogic).'
    // echo "current dir: ${PWD}"
    mvn '-s maven-settings.xml -f ' $ { Constants.APP_MAIN_POM } ' -B -V -U -e com.oracle.weblogic:weblogic-maven-plugin:deploy'
}

void runMavenBuild(boolean enableJacoco) {
    // Apache Maven related side notes:
    // --batch-mode : recommended in CI to inform maven to not run in interactive mode (less logs)
    // -V : strongly recommended in CI, will display the JDK and Maven versions in use.
    //      Very useful to be quickly sure the selected versions were the ones you think.
    // -U : force maven to update snapshots each time (default : once an hour, makes no sense in CI).
    // -Dsurefire.useFile=false : useful in CI. Displays test errors in the logs directly (instead of
    //                            having to crawl the workspace files to see the cause).

    mvn "-s maven-settings.xml -f '${Constants.APP_MAIN_POM}' -B -V -U -e clean install -DskipTests=true"
    archiveArtifacts()
}

void runUnitTests()
{
    mvn "-f '${Constants.APP_MAIN_POM}' -B -V -U -e clean verify -Dsurefire.useFile=false"
    archiveTestResults()
}

@NonCPS
def version(text) {
    def matcher = text =~ '<version>(.+)</version>'
    matcher ? matcher[0][1] : null
}

def mvn(args) {
    def mvnHome = tool 'M3'
    sh "${mvnHome}/bin/mvn ${args}"
}

// Return the contents of this script as object so it can be re-used in Jenkinsfiles.
return this